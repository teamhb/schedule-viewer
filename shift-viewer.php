<?php



    class WorkShift {
        private $id;
    
        public function __construct($id) {
            $this->id = (int) $id;
        }
        
        public function get_shift_date() {
            $shift_date = '2017-01-12'; // fetched from database, based on the $this->id
            return $shift_date;
        }

        public function get_schedule() {
            $shift = array('start_moment'=>'2017-01-12 18:00:00', 
                           'stop_moment'=>'2017-01-13 04:00:00'); // fetched from database, based on the $this->id
            return $shift;
        }

        public function get_punch_pairs() {
        
            // fetched from database, based on the $this->id 
            
            $punch_pairs[] = array('punch_in'=>'2017-01-12 17:53:00', 
                                  'punch_out'=>'2017-01-12 20:00:00');
            
            $punch_pairs[] = array('punch_in'=>'2017-01-12 21:00:00', 
                                  'punch_out'=>'2017-01-13 03:45:00');
                                       

            return $punch_pairs;
        }
        
        public function get_punch_pairs() {
        
            // fetched from database, based on the $this->id 
            
            $segments[] = array('start_moment'=>'2017-01-12 22:00:00', 
                                'stop_moment'=>'2017-01-12 23:00:00', 
                                'segment_code'=>'LUNCH', 
                                'num_hours'=>'1.0000');

            $segments[] = array('start_moment'=>'2017-01-12 21:00:00', 
                                'stop_moment'=>'2017-01-13 00:00:00', 
                                'segment_code'=>'NIGHT PREMIUM', 
                                'num_hours'=>'1.0000');

            $segments[] = array('start_moment'=>'2017-01-13 00:00:00', 
                                'stop_moment'=>'2017-01-13 03:45:00', 
                                'segment_code'=>'NIGHT PREMIUM', 
                                'num_hours'=>'3.7500');

            $segments[] = array('start_moment'=>'2017-01-13 03:45:00', 
                                'stop_moment'=>'2017-01-13 04:00:00', 
                                'segment_code'=>'UNDERTIME', 
                                'num_hours'=>'0.2500');

            return $segments;
        }        
        


    }

    class WorkShiftViewer {
        private $shift; 
        
        public function __construct($id) {
            $this->shift = new WorkShift($id); 
        }
        
        public function render_css() {
            // return any css that relates to this specific shift
        }
        
        public function render_shift($period_start, $period_stop) {
        
           
           // return html for this shift using schedule, punch_pairs and segments
           // will look like this https://awwapp.com/b/urnfcg1u7/
        }
    }

?>

<style type='text/css'>
    <?php echo $shift->render_css(); ?>
</style>

<!-- use boostrap please -->
<div class='row'>
   <div>
       
       <?php echo $shift->render_shift(); ?>
       
       
   </div>
</div>   
